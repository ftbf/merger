# Checking out the code
```
git clone ssh://git@gitlab.cern.ch:7999/ftbf/monicelli.git Monicelli # or via HTTPS or KRB5, as you prefer
cd Monicelli
git checkout TFPX # currently the most up-to-date branch
```


# Installation
```
# update setup.sh for correct MERGER_INPUT_DIR
source setup.sh;
mkdir build;
cd build;
cmake ..;
make;
```

# Running
```
# example usage: `python mergeRuns.py 32944` will merge run 32944
# or: `python mergeRuns.py 32944 32959` will merge all runs from 32944-32959
# or: `python mergeRuns.py 32944 32959 --skip "32945,32947,32948,32952"` will merge all except for the four runs specified
```

Similarly, can use `/data/TestBeam/2022_12_December_T992/copyAndMergeRuns.py` in the same manner, but it also performs the copying
